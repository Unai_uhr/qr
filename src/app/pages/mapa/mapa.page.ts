import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

declare var mapboxgl:any;

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})
export class MapaPage implements OnInit, AfterViewInit {

  lat:number;
  lng: number;

  constructor( private route:ActivatedRoute,private navController:NavController,) { }


  ngAfterViewInit(): void {

    console.log(this.lat);
    console.log(this.lng);
    mapboxgl.accessToken =
      'pk.eyJ1IjoidW5haWhlcm5hbmRleiIsImEiOiJja2k3YXkwZnkwNnF4MzRwOWw1Ymtnc3N3In0.q4r6M_ZiljuaKrLKlC9ECA';
    var map = new mapboxgl.Map({
      style: 'mapbox://styles/mapbox/light-v10',
      center: [this.lng, this.lat],
      zoom: 15.5,
      pitch: 45,
      bearing: -17.6,
      container: 'map',
      antialias: true,
    });
    var marker1 = new mapboxgl.Marker()
      .setLngLat([this.lng,this.lat])
      .addTo(map);

    map.on('load', function () {
      // Insert the layer beneath any symbol layer.
      var layers = map.getStyle().layers;
      var labelLayerId;
      for (var i = 0; i < layers.length; i++) {
        if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
          labelLayerId = layers[i].id;
          break;
        }
      }

      // The 'building' layer in the Mapbox Streets
      // vector tileset contains building height data
      // from OpenStreetMap.
      map.addLayer(
        {
          id: 'add-3d-buildings',
          source: 'composite',
          'source-layer': 'building',
          filter: ['==', 'extrude', 'true'],
          type: 'fill-extrusion',
          minzoom: 15,
          paint: {
            'fill-extrusion-color': '#aaa',

            // Use an 'interpolate' expression to
            // add a smooth transition effect to
            // the buildings as the user zooms in.
            'fill-extrusion-height': [
              'interpolate',
              ['linear'],
              ['zoom'],
              15,
              0,
              15.05,
              ['get', 'height'],
            ],
            'fill-extrusion-base': [
              'interpolate',
              ['linear'],
              ['zoom'],
              15,
              0,
              15.05,
              ['get', 'min_height'],
            ],
            'fill-extrusion-opacity': 0.6,
          },
        },

        labelLayerId
      );
    });

  }

  ngOnInit() {
    let geo:any = this.route.snapshot.paramMap.get('geo');
    geo= geo.substr(4);
    geo= geo.split(',');
    this.lat= Number(geo[0]);
    this.lng= Number(geo[1]);
  }

  back(){
    this.navController.navigateForward('tabs/tab2');
  }

}
